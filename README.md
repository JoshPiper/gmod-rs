[![crates.io](https://img.shields.io/crates/v/gmod.svg)](https://crates.io/crates/gmod)

[![docs.rs](https://docs.rs/gmod/badge.svg)](https://docs.rs/gmod)

# ⚙ gmod-rs

A swiss army knife for creating binary modules for Garry's Mod in Rust.

# Example

[Click here](https://github.com/WilliamVenner/gmod-rs/tree/master/example/my-first-binary-module) to see an example.